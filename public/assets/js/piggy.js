$( document ).ready(function() {
	$('#piggy_identifier').focus();
	$('#piggy_identifier').on('keypress', function(event) {
		var keycode = (event.keyCode ? event.keyCode : event.which);
		if(keycode == '13'){
			var zone_id = $('#zone_id').val();
			var piggy = $("#piggy_identifier").val();
			var data = {'zone_id': zone_id, 'piggy_identifier': piggy};
			send_piggy(data); 
		}
	})
	
});

function send_piggy(data) {
	$.ajax({
		type: "POST",
		url: send_piggy_url,
		data: data,
		success: function(data) {
			$.notify("Piggy Recibido", "success");
		},
		error: function (data) {
			$.notify("Piggy No Recibido", "error");
		},
		complete: function() {
			$('#piggy_identifier').focus();
			$("#piggy_identifier").val("");
		},
		dataType: 'json',
	});
}

