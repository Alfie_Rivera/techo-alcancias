<?php

namespace Fuel\Migrations;

class Create_points
{
	public function up()
	{
		\DBUtil::create_table('points', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'identifier' => array('constraint' => 255, 'type' => 'varchar'),
			'name' => array('constraint' => 255, 'type' => 'varchar'),
			'image' => array('type' => 'text', 'null' => true),
			'zone_id' => array('constraint' => 11, 'type' => 'int'),
			'lat' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),
			'lng' => array('constraint' => 255, 'type' => 'varchar', 'null' => true),
			'volunteers' => array('constraint' => 11, 'type' => 'int'),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('points');
	}
}