<?php

namespace Fuel\Migrations;

class Create_piggies
{
	public function up()
	{
		\DBUtil::create_table('piggies', array(
			'id' => array('constraint' => 11, 'type' => 'int', 'auto_increment' => true, 'unsigned' => true),
			'identifier' => array('constraint' => 255, 'type' => 'varchar'),
			'zone_id' => array('constraint' => 11, 'type' => 'int'),
			'point_id' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'created_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),
			'updated_at' => array('constraint' => 11, 'type' => 'int', 'null' => true),

		), array('id'));
	}

	public function down()
	{
		\DBUtil::drop_table('piggies');
	}
}