<html>
<head>
	<title>Control de Alcancias</title>
	<?php echo Asset::css('bootstrap.css'); ?>
	<style type="text/css">
	body {
		padding-top: 20%;
		background-color: #666;
	}
	</style>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<fieldset>
				<div class="col-md-4 col-md-offset-4">
					<div class="form-group">
						<label>Código de barras:</label>
					<?php echo Form::input('piggy_identifier', '', array('id' => "piggy_identifier",'class' => 'form-control col-md-12', 'autocomplete' => 'off')); ?> 	
					<?php echo Form::input('zone_id', Uri::segment(3,0), array('id' => "zone_id",'class' => 'hidden', 'type'=> 'hidden', 'autocomplete' => 'off')); ?> 
					</div>
				</div>
				</fieldset>
			</div>
		</div>
	</div>
	<script type="text/javascript">
	send_piggy_url = "<?php echo Uri::create('api/send_piggy.json'); ?>";
	</script>
	<?php echo Asset::js(array(
		'jquery.min.js',
		'bootstrap.js',
		'notify.min.js',
		'piggy.js',
	)); ?>
</body>
</html>