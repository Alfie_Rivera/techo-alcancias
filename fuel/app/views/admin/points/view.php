<h2>Viewing #<?php echo $point->id; ?></h2>

<p>
	<strong>Identifier:</strong>
	<?php echo $point->identifier; ?></p>
<p>
	<strong>Name:</strong>
	<?php echo $point->name; ?></p>
<p>
	<strong>Image:</strong>
	<?php echo $point->image; ?></p>
<p>
	<strong>Zone id:</strong>
	<?php echo $point->zone_id; ?></p>
<p>
	<strong>Lat:</strong>
	<?php echo $point->lat; ?></p>
<p>
	<strong>Lng:</strong>
	<?php echo $point->lng; ?></p>
<p>
	<strong>Volunteers:</strong>
	<?php echo $point->volunteers; ?></p>

<?php echo Html::anchor('admin/points/edit/'.$point->id, 'Edit'); ?> |
<?php echo Html::anchor('admin/points', 'Back'); ?>