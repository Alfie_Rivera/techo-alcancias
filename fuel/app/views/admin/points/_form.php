<?php echo Form::open(array("class"=>"form-horizontal")); ?>

	<fieldset>
		<div class="form-group">
			<?php echo Form::label('Identifier', 'identifier', array('class'=>'control-label')); ?>

				<?php echo Form::input('identifier', Input::post('identifier', isset($point) ? $point->identifier : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Identifier')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Name', 'name', array('class'=>'control-label')); ?>

				<?php echo Form::input('name', Input::post('name', isset($point) ? $point->name : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Name')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Image', 'image', array('class'=>'control-label')); ?>

				<?php echo Form::textarea('image', Input::post('image', isset($point) ? $point->image : ''), array('class' => 'col-md-8 form-control', 'rows' => 8, 'placeholder'=>'Image')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Zone', 'zone_id', array('class'=>'control-label')); ?>
				<select name='zone_id'>
					<?php foreach ($zones as $zone): ?>
							<option value="<?php echo $zone->id ?>"><?php echo $zone->name ?></option>
					<?php endforeach ?>
				</select>

		</div>
		<div class="form-group">
			<?php echo Form::label('Lat', 'lat', array('class'=>'control-label')); ?>

				<?php echo Form::input('lat', Input::post('lat', isset($point) ? $point->lat : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Lat')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Lng', 'lng', array('class'=>'control-label')); ?>

				<?php echo Form::input('lng', Input::post('lng', isset($point) ? $point->lng : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Lng')); ?>

		</div>
		<div class="form-group">
			<label class='control-label'>&nbsp;</label>
			<?php echo Form::submit('submit', 'Save', array('class' => 'btn btn-primary')); ?>		</div>
	</fieldset>
<?php echo Form::close(); ?>