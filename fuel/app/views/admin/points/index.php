<h2>Listing Points</h2>
<br>
<?php if ($points): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Identificador</th>
			<th>Nombre</th>
			<th>Zona</th>
			<th>Voluntarios</th>
			<th>Alcancias</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($points as $item): ?>		<tr>

			<td><?php echo $item->identifier; ?></td>
			<td><?php echo $item->name; ?></td>
			<td><?php echo (isset($item->zone->name)) ? $item->zone->name : 'Sin asignar'; ?></td>
			<td><?php echo $item->volunteers; ?></td>
			<td><?php echo Controller_Admin_Points::total($item->id); ?></td>
			<td>
				<?php echo Html::anchor('admin/points/view/'.$item->id, 'View'); ?> |
				<?php echo Html::anchor('admin/points/edit/'.$item->id, 'Edit'); ?> |
				<?php echo Html::anchor('admin/points/delete/'.$item->id, 'Delete', array('onclick' => "return confirm('Are you sure?')")); ?>

			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>No Points.</p>

<?php endif; ?><p>
	<?php echo Html::anchor('admin/points/create', 'Add new Point', array('class' => 'btn btn-success')); ?>

</p>
