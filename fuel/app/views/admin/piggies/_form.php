<?php echo Form::open(array("class"=>"form-horizontal")); ?>

	<fieldset>
		<div class="form-group">
			<?php echo Form::label('Identifier', 'identifier', array('class'=>'control-label')); ?>

				<?php echo Form::input('identifier', Input::post('identifier', isset($piggy) ? $piggy->identifier : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Identifier')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Zone', 'zone_id', array('class'=>'control-label')); ?>
				<select name='zone_id'>
					<?php foreach ($zones as $zone): ?>
							<option value="<?php echo $zone->id ?>"><?php echo $zone->name ?></option>
					<?php endforeach ?>
				</select>
		</div>
		<div class="form-group">
			<?php echo Form::label('Point id', 'point_id', array('class'=>'control-label')); ?>
				<select name='point_id'>
					<?php foreach ($points as $point): ?>
							<option value="<?php echo $point->id ?>"><?php echo $point->name ?></option>
					<?php endforeach ?>
				</select>
		</div>
		<div class="form-group">
			<label class='control-label'>&nbsp;</label>
			<?php echo Form::submit('submit', 'Save', array('class' => 'btn btn-primary')); ?>		</div>
	</fieldset>
<?php echo Form::close(); ?>