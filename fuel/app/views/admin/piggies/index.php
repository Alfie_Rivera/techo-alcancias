<h2>Listing Piggies</h2>
<br>
<?php if ($piggies): ?>
<table class="table table-striped">
	<thead>
		<tr>
			<th>Identifier</th>
			<th>Zona</th>
			<th>Punto</th>
			<th></th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($piggies as $item): ?>		<tr>

			<td><?php echo $item->identifier; ?></td>
			<td><?php echo (isset($item->zone->name)) ? $item->zone->name : 'Sin Asignar'; ?></td>
			<td><?php echo (isset($item->point->identifier)) ? $item->point->identifier : 'Sin Asignar'; ?></td>
			<td>
				<?php echo Html::anchor('admin/piggies/view/'.$item->id, 'View'); ?> |
				<?php echo Html::anchor('admin/piggies/edit/'.$item->id, 'Edit'); ?> |
				<?php echo Html::anchor('admin/piggies/delete/'.$item->id, 'Delete', array('onclick' => "return confirm('Are you sure?')")); ?>

			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>No Piggies.</p>

<?php endif; ?><p>
	<?php echo Html::anchor('admin/piggies/create', 'Add new Piggy', array('class' => 'btn btn-success')); ?>

</p>
