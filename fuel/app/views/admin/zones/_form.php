<?php echo Form::open(array("class"=>"form-horizontal")); ?>

	<fieldset>
		<div class="form-group">
			<?php echo Form::label('Nombre', 'name', array('class'=>'control-label')); ?>

				<?php echo Form::input('name', Input::post('name', isset($zone) ? $zone->name : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Name')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Color', 'color', array('class'=>'control-label')); ?>
			<br>
				<?php echo Form::hidden('color', Input::post('color', isset($zone) ? $zone->color : ''), array('class' => 'col-md-4 form-control colorpicker', 'placeholder'=>'Color')); ?>

		</div>
		<div class="form-group">
			<?php echo Form::label('Total', 'total', array('class'=>'control-label')); ?>

				<?php echo Form::input('total', Input::post('total', isset($zone) ? $zone->total : ''), array('class' => 'col-md-4 form-control', 'placeholder'=>'Total')); ?>

		</div>
		<div class="form-group">
			<label class='control-label'>&nbsp;</label>
			<?php echo Form::submit('submit', 'Salvar', array('class' => 'btn btn-primary')); ?>		</div>
	</fieldset>
<?php echo Form::close(); ?>