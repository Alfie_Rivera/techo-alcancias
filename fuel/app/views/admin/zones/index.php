<h2>Listado de Zonas</h2>
<br>
<?php if ($zones): ?>
<table class="table">
	<thead>
		<tr>
			<th>Nombre</th>
			<th>Total Alcancias</th>
			<th>Total Voluntarios</th>
			<th style="width:190px; text-align:center;"></th>
		</tr>
	</thead>
	<tbody>
<?php foreach ($zones as $item): ?>		<tr style="background:<?php echo $item->color; ?> !important;">

			<td><?php echo $item->name; ?></td>
			<td><?php echo Controller_Admin_Zones::total($item->id); ?></td>
			<td><?php echo Controller_Admin_Zones::volunteers($item->id); ?></td>
			<td>
				<?php echo Html::anchor('api/deliver/'.$item->id, 'Entregar', array('target' => '_blank')); ?> |
				<?php echo Html::anchor('admin/zones/edit/'.$item->id, 'Editar'); ?> |
				<?php echo Html::anchor('admin/zones/delete/'.$item->id, 'Eliminar', array('onclick' => "return confirm('¿Seguro?')")); ?>

			</td>
		</tr>
<?php endforeach; ?>	</tbody>
</table>

<?php else: ?>
<p>No Zones.</p>

<?php endif; ?><p>
	<?php echo Html::anchor('admin/zones/create', 'Nueva Zona', array('class' => 'btn btn-success')); ?>

</p>
