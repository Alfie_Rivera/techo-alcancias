<?php
class Controller_Admin_Piggies extends Controller_Admin{

	public function action_index()
	{
		$data['piggies'] = Model_Piggy::find('all');
		$this->template->title = "Piggies";
		$this->template->content = View::forge('admin/piggies/index', $data);

	}

	public function action_view($id = null)
	{
		$data['piggy'] = Model_Piggy::find($id);

		$this->template->title = "Piggy";
		$this->template->content = View::forge('admin/piggies/view', $data);

	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Piggy::validate('create');

			if ($val->run())
			{
				$piggy = Model_Piggy::forge(array(
					'identifier' => Input::post('identifier'),
					'zone_id' => Input::post('zone_id'),
					'point_id' => Input::post('point_id'),
				));

				if ($piggy and $piggy->save())
				{
					Session::set_flash('success', e('Added piggy #'.$piggy->id.'.'));

					Response::redirect('admin/piggies');
				}

				else
				{
					Session::set_flash('error', e('Could not save piggy.'));
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}
		$zones = Model_Zone::query()->get();
		$points = Model_Point::query()->get();
		$this->template->set_global('points', $points);
		$this->template->set_global('zones', $zones);
		$this->template->title = "Piggies";
		$this->template->content = View::forge('admin/piggies/create');

	}

	public function action_edit($id = null)
	{
		$piggy = Model_Piggy::find($id);
		$val = Model_Piggy::validate('edit');

		if ($val->run())
		{
			$piggy->identifier = Input::post('identifier');
			$piggy->zone_id = Input::post('zone_id');
			$piggy->point_id = Input::post('point_id');

			if ($piggy->save())
			{
				Session::set_flash('success', e('Updated piggy #' . $id));

				Response::redirect('admin/piggies');
			}

			else
			{
				Session::set_flash('error', e('Could not update piggy #' . $id));
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$piggy->identifier = $val->validated('identifier');
				$piggy->zone_id = $val->validated('zone_id');
				$piggy->point_id = $val->validated('point_id');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('piggy', $piggy, false);
		}
		$zones = Model_Zone::query()->get();
		$points = Model_Point::query()->get();
		$this->template->set_global('zones', $zones);
		$this->template->set_global('points', $points);
		$this->template->title = "Piggies";
		$this->template->content = View::forge('admin/piggies/edit');

	}

	public function action_delete($id = null)
	{
		if ($piggy = Model_Piggy::find($id))
		{
			$piggy->delete();

			Session::set_flash('success', e('Deleted piggy #'.$id));
		}

		else
		{
			Session::set_flash('error', e('Could not delete piggy #'.$id));
		}

		Response::redirect('admin/piggies');

	}


}