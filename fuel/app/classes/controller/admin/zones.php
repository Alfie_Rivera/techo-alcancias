<?php
class Controller_Admin_Zones extends Controller_Admin{

	public function action_index()
	{
		$data['zones'] = Model_Zone::find('all');
		$this->template->title = "Zonas";
		$this->template->content = View::forge('admin/zones/index', $data);

	}

	public function action_view($id = null)
	{
		$data['zone'] = Model_Zone::find($id);

		$this->template->title = "Zona";
		$this->template->content = View::forge('admin/zones/view', $data);

	}

	public static function total($id)
	{
		return count(Model_Piggy::query()->where('zone_id', $id)->get());
	}

	public static function volunteers($id)
	{
		$query = DB::query("SELECT SUM(volunteers) as total FROM points WHERE zone_id = ".$id)->execute()[0]['total'];
		return ($query) ? $query : 0;
	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Zone::validate('create');

			if ($val->run())
			{
				$zone = Model_Zone::forge(array(
					'name' => Input::post('name'),
					'color' => Input::post('color'),
					'total' => Input::post('total'),
				));

				if ($zone and $zone->save())
				{
					Session::set_flash('success', e('Added zone #'.$zone->id.'.'));

					Response::redirect('admin/zones');
				}

				else
				{
					Session::set_flash('error', e('Could not save zone.'));
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}

		$this->template->title = "Zonas";
		$this->template->content = View::forge('admin/zones/create');

	}

	public function action_edit($id = null)
	{
		$zone = Model_Zone::find($id);
		$val = Model_Zone::validate('edit');

		if ($val->run())
		{
			$zone->name = Input::post('name');
			$zone->color = Input::post('color');
			$zone->total = Input::post('total');

			if ($zone->save())
			{
				Session::set_flash('success', e('Updated zone #' . $id));

				Response::redirect('admin/zones');
			}

			else
			{
				Session::set_flash('error', e('Could not update zone #' . $id));
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$zone->name = $val->validated('name');
				$zone->color = $val->validated('color');
				$zone->total = $val->validated('total');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('zone', $zone, false);
		}

		$this->template->title = "Zonas";
		$this->template->content = View::forge('admin/zones/edit');

	}

	public function action_delete($id = null)
	{
		if ($zone = Model_Zone::find($id))
		{
			$zone->delete();

			Session::set_flash('success', e('Deleted zone #'.$id));
		}

		else
		{
			Session::set_flash('error', e('Could not delete zone #'.$id));
		}

		Response::redirect('admin/zones');

	}


}