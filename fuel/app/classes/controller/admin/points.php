<?php
class Controller_Admin_Points extends Controller_Admin{

	public function action_index()
	{
		$data['points'] = Model_Point::find('all');
		$this->template->title = "Points";
		$this->template->content = View::forge('admin/points/index', $data);

	}

	public function action_view($id = null)
	{
		$data['point'] = Model_Point::find($id);

		$this->template->title = "Point";
		$this->template->content = View::forge('admin/points/view', $data);

	}

	public static function total($id)
	{
		$query = DB::query("SELECT COUNT(*) as total FROM piggies WHERE point_id = ".$id)->execute()[0]['total'];
		return ($query) ? $query : 0;
	}

	public function action_create()
	{
		if (Input::method() == 'POST')
		{
			$val = Model_Point::validate('create');

			if ($val->run())
			{
				$point = Model_Point::forge(array(
					'identifier' => Input::post('identifier'),
					'name' => Input::post('name'),
					'image' => Input::post('image'),
					'zone_id' => Input::post('zone_id'),
					'lat' => Input::post('lat'),
					'lng' => Input::post('lng'),
					'volunteers' => Input::post('volunteers'),
				));

				if ($point and $point->save())
				{
					Session::set_flash('success', e('Added point #'.$point->id.'.'));

					Response::redirect('admin/points');
				}

				else
				{
					Session::set_flash('error', e('Could not save point.'));
				}
			}
			else
			{
				Session::set_flash('error', $val->error());
			}
		}
		$zones = Model_Zone::query()->get();
		$this->template->set_global('zones', $zones);
		$this->template->title = "Points";
		$this->template->content = View::forge('admin/points/create');

	}

	public function action_edit($id = null)
	{
		$point = Model_Point::find($id);
		$val = Model_Point::validate('edit');
		if ($val->run())
		{
			$point->identifier = Input::post('identifier');
			$point->name = Input::post('name');
			$point->image = Input::post('image');
			$point->zone_id = Input::post('zone_id');
			$point->lat = Input::post('lat');
			$point->lng = Input::post('lng');
			$point->volunteers = Input::post('volunteers');

			if ($point->save())
			{
				Session::set_flash('success', e('Updated point #' . $id));

				Response::redirect('admin/points');
			}

			else
			{
				Session::set_flash('error', e('Could not update point #' . $id));
			}
		}

		else
		{
			if (Input::method() == 'POST')
			{
				$point->identifier = $val->validated('identifier');
				$point->name = $val->validated('name');
				$point->image = $val->validated('image');
				$point->zone_id = $val->validated('zone_id');
				$point->lat = $val->validated('lat');
				$point->lng = $val->validated('lng');
				$point->volunteers = $val->validated('volunteers');

				Session::set_flash('error', $val->error());
			}

			$this->template->set_global('point', $point, false);
		}
		$zones = Model_Zone::query()->get();
		$this->template->set_global('zones', $zones);
		$this->template->title = "Points";
		$this->template->content = View::forge('admin/points/edit');

	}

	public function action_delete($id = null)
	{
		if ($point = Model_Point::find($id))
		{
			$point->delete();

			Session::set_flash('success', e('Deleted point #'.$id));
		}

		else
		{
			Session::set_flash('error', e('Could not delete point #'.$id));
		}

		Response::redirect('admin/points');

	}


}