<?php

class Controller_Api extends Controller_Hybrid
{

	public $format = 'json';
	
	public function get_piggy($id=null) {
		if ($id) {
			$piggies = Model_Piggy::query()->where('identifier', $id)->get();
		} else {
			$piggies = Model_Piggy::query()->get();
		}
		return $this->response(array('data'=> $piggies));
	}

	public function post_piggy(){
		$piggy = Model_Piggy::forge();
		$piggy->identifier = Input::post('identifier');
		$piggy->zone_id = Input::post('zone_id');
		$piggy->point_id = Input::post('point_id');
		$piggy->save();
		return $this->response(array('msg' => "success"));
	}

	public function get_point($id=null) {
		if ($id) {
			$points = Model_Point::query()->where('identifier', $id)->get();
		} else {
			$points = Model_Point::query()->get();
		}
		return $this->response(array('data'=> $points));
	}

	public function post_point(){
		$point = Model_Point::forge();
		$point->identifier = Input::post('identifier');
		$point->name = Input::post('name');
		$point->image = Input::post('image');
		$point->zone_id = Input::post('zone_id');
		$point->lat = Input::post('lat');
		$point->lng = Input::post('lng');
		$point->volunteers = Input::post('volunteers');
		$point->save();
		return $this->response(array('msg' => "success"));

	}

	public function get_zone($id=null) {
		Log::info("here!");
		if ($id) {
			$zones = Model_Zone::query()->where('id', $id)->get();
		} else {
			Log::info('lest');
			$zones = Model_Zone::query()->get();
		}
		return $this->response(array('data'=> $zones));
	}

	public function post_zone(){
		$zone = Model_Zone::forge();
		$zone->name = Input::post('name');
		$zone->color = Input::post('color');
		$zone->total = Input::post('total');
		$zone->save();
		return $this->response(array('msg' => "success"));

	}
	public function action_deliver()
	{
		return View::forge('interface/general');
	}

	public function post_send_piggy(){
		$zone_id = Input::post('zone_id',0);
		$identifier = Input::post('piggy_identifier');
		$point = Input::post('point_id', 0);
        if (empty($identifier)) {
            Log::info('comuasi');
            $response = new Response("Indentificador Vacio", 500);
            return $response;
        }

		$piggy = Model_Piggy::query()->where('identifier', $identifier)->get_one();
		if (!$piggy) {
			$piggy = Model_Piggy::forge();
			$piggy->identifier = $identifier;
		}
		$piggy->zone_id = $zone_id;
		$piggy->point_id = $point;
		$piggy->save();
        return $piggy;
	}

	public function post_volunteers($id)
	{
		$point = Model_Point::query()->where('identifier', $id)->get_one();
		$data = [];
		$data['msg'] = 'Point not found.';
		
		if ($point) {
			$point->volunteers = Input::post('volunteers');
			$point->save();
			$data['msg'] = 'Point Updated!';
		}
		return $this->response($data);
	}

}