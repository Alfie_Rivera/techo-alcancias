<?php
class Model_Point extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'identifier',
		'name',
		'image',
		'zone_id',
		'lat',
		'lng',
		'volunteers',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_belongs_to = array(
		'zone' => array(
			'key_from' => 'zone_id',
			'model_to' => 'Model_Zone',
			'key_to' => 'id',
			'cascade_save' => false,
			'cascade_delete' => false,
	)
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('identifier', 'Identifier', 'required|max_length[255]');
		$val->add_field('name', 'Name', 'required|max_length[255]');
		$val->add_field('image', 'Image', 'required');
		$val->add_field('zone_id', 'Zone Id', 'required|valid_string[numeric]');
		$val->add_field('lat', 'Lat', 'required|max_length[255]');
		$val->add_field('lng', 'Lng', 'required|max_length[255]');
		$val->add_field('volunteers', 'Volunteers', 'required|valid_string[numeric]');

		return $val;
	}

}
