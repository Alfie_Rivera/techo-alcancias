<?php
class Model_Piggy extends \Orm\Model
{
	protected static $_properties = array(
		'id',
		'identifier',
		'zone_id',
		'point_id',
		'created_at',
		'updated_at',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_save'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_belongs_to = array(
		'zone' => array(
			'key_from' => 'zone_id',
			'model_to' => 'Model_Zone',
			'key_to' => 'id',
			'cascade_save' => false,
			'cascade_delete' => false,
		),
		'point' => array(
			'key_from' => 'point_id',
			'model_to' => 'Model_Point',
			'key_to' => 'id',
			'cascade_save' => false,
			'cascade_delete' => false,
		),
	);

	public static function validate($factory)
	{
		$val = Validation::forge($factory);
		$val->add_field('identifier', 'Identifier', 'required|max_length[255]');
		$val->add_field('zone_id', 'Zone Id', 'required|valid_string[numeric]');
		$val->add_field('point_id', 'Point Id', 'required|valid_string[numeric]');

		return $val;
	}

}
